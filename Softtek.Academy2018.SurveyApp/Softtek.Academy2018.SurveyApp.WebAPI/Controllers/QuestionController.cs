﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Business.Implementation;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Data.Implementations;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.SurveyApp.WebAPI.Controllers
{

    [RoutePrefix("api/Question")]
    public class QuestionController : ApiController
    {
        private readonly IQuestionService _questionService;
        public QuestionController()
        {
            IQuestionRepository repository = new QuestionDataRepository();
            _questionService = new QuestionService(repository);
        }
        [Route("")]
        [HttpPost]
        public IHttpActionResult CreateSurvey([FromBody] QuestionDTO quesitonDTO)
        {
            if (quesitonDTO == null) return BadRequest("Request is null");
            Question question = new Question
            {
                Text = quesitonDTO.Text,
                IsActive = quesitonDTO.IsActive,
                QuestionTypeId = quesitonDTO.QuestionTypeId,
            };
            int id = _questionService.Add(question);
            if (id <= 0) return BadRequest("Unable to create User)");
            var payload = new { UserId = id };
            return Ok(payload);
        }
        
    }
}
