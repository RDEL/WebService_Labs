﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Business.Implementation;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Data.Implementations;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.SurveyApp.WebAPI.Controllers
{
    [RoutePrefix("api/Survey")]
    public class SurveyController : ApiController
    {
        private readonly ISurveyService _SurveySevice;
        public SurveyController()
        {
            ISurveyRepository repository = new SurveyDataRepository();
            _SurveySevice = new SurveyService(repository);
        }


        [Route("")]
        [HttpPost]
        public IHttpActionResult CreateSurvey([FromBody] SurveyDTO surveyDTO)
        {
            if (surveyDTO == null) return BadRequest("Request is null");
            Survey survey = new Survey
            {
                Title = surveyDTO.Title,
                Description = surveyDTO.Description,
               IsArchived = surveyDTO.IsArchived,
            };
            int id = _SurveySevice.Add(survey);
            if (id <= 0) return BadRequest("Unable to create User)");
            var payload = new { UserId = id };
            return Ok(payload);
        }

        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult GetSurveyId([FromUri] int id)
        {
            Survey survey = _SurveySevice.GetSurveyId(id);

            SurveyDTO surveyDTO = new SurveyDTO
            {
                Title = survey.Title,
                Description = survey.Description,
                 IsArchived = survey.IsArchived
            };
            return Ok(surveyDTO);
        }

        [Route("{id:int}")]
        [HttpPut]
        public IHttpActionResult UpdateSurvey([FromUri] int id, [FromBody] SurveyDTO surveyDTO)
        {
            if (surveyDTO == null) return BadRequest("User is null");

            Survey user = new Survey
            {
                Id = id,
                Title = surveyDTO.Title,
                Description = surveyDTO.Description,
                IsArchived = surveyDTO.IsArchived,
                ModifiedDate = surveyDTO.ModifiedDate
            };

            bool result = _SurveySevice.Update(user);
            if (!result) return BadRequest("Update Fail");

            return Ok();
        }



    }
}
