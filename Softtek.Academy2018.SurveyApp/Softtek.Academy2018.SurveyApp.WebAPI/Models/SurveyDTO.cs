﻿using System;

namespace Softtek.Academy2018.SurveyApp.WebAPI.Controllers
{
    public class SurveyDTO
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public bool IsArchived { get; set; }

        public DateTime CreatedDate { get; set; }


        public DateTime ModifiedDate { get; set; }

        //public virtual ICollection<Question> Questions { get; set; }
    }
}