﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Data.Entity;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class OptionDataRepository : IOptionRepository
    {
        public int Add(Option survey)
        {
            using (var ctx = new SuerveyDbContext())
            {
                survey.CreatedDate = DateTime.Now;
                ctx.SaveChanges();
                return survey.Id;
            }
        }

        public bool Delete(Option survey)
        {
            throw new NotImplementedException();
        }

        public bool DoYouHaveQuestion(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Questions.Any(x => x.Options.Any(p => p.Id == id));
            }
        }

        public bool Exist(int id)
        {
            throw new NotImplementedException();
        }

        public Option Get(int id)
        {
            throw new NotImplementedException();
        }

        public bool Repeat(string text)
        {
            using (var ctx = new SuerveyDbContext())
            {
                Option currentOption = ctx.Options.SingleOrDefault(x => x.Text == text);

                if (currentOption == null) return false;

                return true;
            }

        }

        public bool Update(Option survey)
        {
            using (var ctx = new SuerveyDbContext())
            {
                Option currentOption = ctx.Options.SingleOrDefault(x => x.Id == survey.Id);

                if (currentOption == null) return false;


                currentOption.Text = survey.Text;
                currentOption.ModifiedDate = DateTime.Now;
                ctx.Entry(currentOption).State = EntityState.Modified;
                ctx.SaveChanges();

                return true;
            }
        }
    }
}
