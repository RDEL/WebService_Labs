﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Data.Entity;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class QuestionDataRepository : IQuestionRepository
    {
        public int Add(Question survey)
        {
            using (var ctx = new SuerveyDbContext())
            {
                survey.CreatedDate = DateTime.Now;
                ctx.SaveChanges();
                return survey.Id;
            }
        }

        public bool Delete(Question survey)
        {
            throw new NotImplementedException();
        }

        public bool Exist(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Options.Any(e => e.Id == id);
            }
        }

        public Question Get(int id)
        {
            throw new NotImplementedException();
        }

        public bool DoYouHaveSurvey(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Surveys.Any(x => x.Questions.Any(p => p.Id == id));
            }

        }

        public bool Update(Question survey)
        {
            using (var ctx = new SuerveyDbContext())
            {
                Question currentQuestion = ctx.Questions.SingleOrDefault(x => x.Id == survey.Id);

                if (currentQuestion == null) return false;
                
                currentQuestion.IsActive = survey.IsActive;
                currentQuestion.Text = survey.Text;
                currentQuestion.QuestionTypeId = survey.QuestionTypeId;
                currentQuestion.ModifiedDate = DateTime.Now;

                ctx.Entry(currentQuestion).State = EntityState.Modified;
                ctx.SaveChanges();

                return true;
            }
        }
    }
}
