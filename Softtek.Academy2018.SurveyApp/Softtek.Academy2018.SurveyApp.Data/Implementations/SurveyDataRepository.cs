﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Data.Entity;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class SurveyDataRepository : ISurveyRepository
    {
        public int Add(Survey survey)
        {
            using ( var ctx = new SuerveyDbContext())
            {
                survey.IsArchived = true;
                survey.CreatedDate = DateTime.Now;
                ctx.Surveys.Add(survey);
                ctx.SaveChanges();
                return survey.Id;

            }
        }

        public bool Delete(Survey survey)
        {
            throw new NotImplementedException();
        }

        public bool Exists(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Surveys.Any(e => e.Id == id);
            }
        }

        public Survey Get(int id)
        {
            using (var ctx = new SuerveyDbContext())
            {
                return ctx.Surveys.AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        public bool Update(Survey survey)
        {
            using (var ctx = new SuerveyDbContext())
            {
                Survey currentSurvey = ctx.Surveys.SingleOrDefault(x => x.Id == survey.Id);

                if (currentSurvey == null) return false;
                currentSurvey.Title = survey.Title;
                currentSurvey.Description = survey.Description;
                currentSurvey.IsArchived = survey.IsArchived;
                currentSurvey.ModifiedDate = DateTime.Now;
                ctx.Entry(currentSurvey).State = EntityState.Modified;
                ctx.SaveChanges();
                return true;
            }
        }
    }
}
