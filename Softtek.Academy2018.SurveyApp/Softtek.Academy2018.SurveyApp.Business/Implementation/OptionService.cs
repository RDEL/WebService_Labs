﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.Data.Contracts;

namespace Softtek.Academy2018.SurveyApp.Business.Implementation
{
    public class OtionService : IOptionService
    {
        private readonly IOptionRepository _dataRepo;

        public OtionService(IOptionRepository dataRepo)
        {
            _dataRepo = dataRepo;
        }

        public int Add(Option option)
        {
            bool repeat = _dataRepo.Repeat(option.Text);
            if (!repeat) return 0;

            int id = _dataRepo.Add(option);
            return id;
        }

        public bool Update(Option option)
        {
            //Una opción no puede ser modificada si es parte de una pregunta

            bool exists = _dataRepo.Exist(option.Id);
            if (!exists) return false;
            bool HaveQuestion = _dataRepo.DoYouHaveQuestion(option.Id);
            if (HaveQuestion) return false;
            bool update = _dataRepo.Update(option);
            return update;
        }
    }
}
