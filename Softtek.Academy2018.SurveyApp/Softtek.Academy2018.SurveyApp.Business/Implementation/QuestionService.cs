﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.Data.Contracts;

namespace Softtek.Academy2018.SurveyApp.Business.Implementation
{
    public class QuestionService : IQuestionService
    {

        private readonly IQuestionRepository _dataRepo;

        public QuestionService(IQuestionRepository dataRepo)
        {
            _dataRepo = dataRepo;
        }

        public int Add(Question question)
        {
            


            int id = _dataRepo.Add(question);
            return id;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(Question question)
        {
            //una pregunta no puede ser modificada si tiene una pregunta asigndaa
            bool HaveSurvey = _dataRepo.DoYouHaveSurvey(question.Id);
            if (HaveSurvey) return false;

            bool exists = _dataRepo.Exist(question.QuestionTypeId);
            if (!exists) return false;

            bool update = _dataRepo.Update(question);

            return update;

        }
    }
}
