﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Contracts
{
    public interface ISurveyService
    {
        int Add(Survey survey);
        int Delete(int id);
        Survey GetSurveyId(int id);
        bool Update(Survey survey);


    }
}
